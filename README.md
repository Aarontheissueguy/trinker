# 🎮 Trinker (WIP, Contributions Needed!)

An open and easily extendible cross-platform drinking game made with [BeeWare](https://beeware.org/) and love.

Trinker aims to deliver everything a drinking game needs and nothing it does not. No ads, no paywalls, free as in freedom.

![Screenshot 1](trinker/screenshots/screenshot1.png) ![Screenshot 2](trinker/screenshots/screenshot2.png)

## 🚀 Features:

* Unlimited number of players
* Supports different gamemodes/task catalogues (never have I ever, truth or dare, ...)
* Multiple choice questions
* Easy syntax to contribute tasks and questions
* Currently available for Android(Know your limits!, 18+) : [Android(apk)](https://gitlab.com/Aarontheissueguy/trinker/-/releases/Release)

## 🤝 Contribution

### 💻 Code Contributions:
Code contributions are welcome. Setting things up should be a matter of installing BeeWare and its tools. No additional dependencies are needed.

### 🎨 No-Code Contributions
Not a programmer? No problem!

Trinker is more than just software. We need contributors who [translate](trinker/src/trinker/locales/translate.md) Trinker and create [new gamemodes and tasks](trinker/src/trinker/gamemodes/gamemode.md) in general. Click the links to learn more 😉

### 📜 Code of Conduct

1. **Respect**: Treat all players with respect and kindness. Do not propose hateful tasks, questions, or gamemodes. This includes but is not limited to sexism, racism, and homophobia.

2. **Consent**: Drinking games should be fun for everyone, don't propose tasks that require actions without the consent of a player. Example: "Player1 chooses a player to kiss/touch/...". Such tasks may only ever exist in gamemodes explicitly marked as intimate and should use formulations like "Player1 may kiss another player that agrees" or similar.

3. **Limits**: Tasks, questions, and gamemodes should not specifically aim to make people as drunk as possible, as quickly as possible.

4. **Other Drugs**: Trinker is generally open to including gamemodes for soft drugs but may refuse official support, depending on the substance, legality, and other considerations.

## 🗺️ Roadmap to Release
Base functionality of the project is established but there are still hurdles to overcome. An app store release will not happen until this to-do list is complete.

1: [X] [Add questions, tasks, and gamemodes (help needed)](trinker/src/trinker/gamemodes/gamemode.md)

2: [X] Real-world testing

3: [X] Polish user experience

4: [ ] Initial release on F-Droid
