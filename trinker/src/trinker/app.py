import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW, CENTER, BOLD, MONOSPACE, SANS_SERIF
import os
from os.path import isfile, join
from . import tools
from toga.colors import rgb

import gettext
import os
import time

# Set the path to the 'locales' directory
localedir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'locales'))

language = "en"
translation = gettext.translation('trinker', localedir, languages=[language])
translation.install()
current_language = translation.info()['language']
print("Current language:", current_language)

class Trinker(toga.App):
    def startup(self):
        self.current_language = translation.info()['language']
        self.player_count = 0
        self.counter = 0
        self.player_list = []
        self.app_directory = os.path.dirname(os.path.abspath(__file__))

        toga.Font.register("font", "resources/MouldyCheeseRegular-WyMWG.ttf")

        rgb_colors = [
            rgb(239, 71, 111),  # removebutton and back
            rgb(63,81,181),  # bgcolor
            rgb(246,217,107),  # toolbar and buttons
            rgb(0,0,0), # text buttons
            rgb(255, 255, 255) # text remove and back buttons + labels
        ]
        self.main_box = toga.Box(style=Pack(direction=COLUMN, background_color=rgb_colors[1]))

        def language_callback():
            self.current_language = tools.language()
            translation = gettext.translation('trinker', localedir, languages=[self.current_language])
            translation.install()
            self.sub_welcome_label.text = _('A drinking game made with 🐝')
            self.name_button.text = _('Add Player')
            self.start_button.text = _('Start')
            self.button.text = _('Next')
            if str(self.current_language) in os.listdir(os.path.join(self.app_directory, "gamemodes/")):
                self.mode_select.items = [f"🎉 {os.path.splitext(f)[0]}" for f in os.listdir(
                    os.path.join(self.app_directory, "gamemodes/" + str(self.current_language) + "/")) if isfile(
                    join(os.path.join(self.app_directory, "gamemodes/" + str(self.current_language) + "/"),
                         f)) and f.endswith('.txt')]
            else:
                self.mode_select.items = [f"🎉 {os.path.splitext(f)[0]}" for f in os.listdir(
                    os.path.join(self.app_directory, "gamemodes/" + "en")) if isfile(
                    join(os.path.join(self.app_directory, "gamemodes/" + "en"), f)) and f.endswith('.txt')]

            while self.player_box.children:
                self.player_box.remove(self.player_box.children[-1])

            tools.box_switch(self, self.name_box)

        # The entirety of name_box and its callbacks
        def start_callback():
            self.player_list = tools.get_player_list(self)

            if self.player_list != []:
                self.player_list = tools.get_player_list(self)
                tools.box_switch(self, self.task_box)
                tools.task_handler(self, )

            else:
                self.sub_welcome_label.text = _('Press the "add player" button!')
        def name_callback():
            self.player_count = 0
            for i in tools.get_player_list(self):
                self.player_count += 1

            print(self.player_count)
            name_widget(self)
        def name_widget(self):

            input_field_box = toga.Box(style=Pack(direction=ROW, padding=5))  # Create a box for each input field
            # Using negative padding to closer match the size of the remove button on android
            input_field = toga.TextInput(placeholder=_('name'), style=Pack(flex=1, padding_top=-3, padding_bottom=-3))
            # container is neccesary for theming
            input_field_container = toga.Box(style=Pack(direction=ROW, background_color=rgb_colors[4], flex=4, padding=5))
            input_field_container.add(input_field)
            input_field_box.add(input_field_container)  # Add the input field to its box

            # Create an "❌" button and add it to the input field box with a callback to remove both input field and button
            def remove_callback(parent):
                self.player_box.remove(parent)
            remove_button = toga.Button(_("⌫ remove"), style=Pack(flex=1, padding_left=5, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb_colors[0], color=rgb(255,255,255)), on_press=lambda widget: remove_callback(remove_button.parent))

            input_field_box.add(remove_button)  # Add the "❌" button to the input field box
            self.player_box.add(input_field_box)  # Add the input field box to the player_box

        self.logo = toga.Image("resources/trinkspiel.png")
        self.logo_box = toga.Box(style=Pack(direction=ROW))
        self.placeholderL = toga.Box(style=Pack(flex=1))
        self.placeholderR = toga.Box(style=Pack(flex=1))
        self.view = toga.ImageView(self.logo, style=Pack(width=300, flex=1))
        self.logo_box.add(self.placeholderL)
        self.logo_box.add(self.view)
        self.logo_box.add(self.placeholderR)
        self.sub_welcome_label = toga.Label(_("A drinking game made with 🐝"), style=Pack(font_family="font", font_size=20, text_align=CENTER, color=rgb_colors[4]))
        # mode select will display every file in the gamemodes folder that ends with txt. It removes the .txt and adds an emoji, This is a shitty way to do it because it needs to be reverted. It should really take a list and modify that but I cant be bothered rn.
        #this container is neccesary to allow theming
        self.mode_select_container = toga.Box(style=Pack(direction=ROW, background_color=rgb_colors[4], padding_top=5, padding_bottom=5, padding_left=10, padding_right=10))
        if str(self.current_language) in os.listdir(os.path.join(self.app_directory, "gamemodes/")):
            self.mode_select = toga.Selection(items=[f"🎉 {os.path.splitext(f)[0]}" for f in os.listdir(os.path.join(self.app_directory, "gamemodes/" + str(self.current_language) + "/")) if isfile(join(os.path.join(self.app_directory, "gamemodes/" + str(self.current_language) + "/"), f)) and f.endswith('.txt')], style=Pack(flex=1, padding=5, color=rgb_colors[4]))
        else:
            self.mode_select = toga.Selection(items=[f"🎉 {os.path.splitext(f)[0]}" for f in os.listdir(os.path.join(self.app_directory, "gamemodes/" + "en")) if isfile(join(os.path.join(self.app_directory, "gamemodes/" + "en"), f)) and f.endswith('.txt')], style=Pack(flex=1, padding=5, color=rgb_colors[4]))
        self.mode_select_container.add(self.mode_select)
        self.name_lang_box = toga.Box(style=Pack(direction=ROW))
        self.name_button = toga.Button(_("Add player"), on_press=lambda widget: name_callback(), style=Pack(padding=5, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb_colors[2], color=rgb_colors[3], flex=4))
        self.language_button = toga.Button(_("🌍"), on_press=lambda widget: language_callback(), style=Pack(padding=5, padding_left=0, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb_colors[2], color=rgb_colors[3], flex=1))
        self.name_lang_box.add(self.name_button)
        self.name_lang_box.add(self.language_button)
        self.start_button = toga.Button("Start",on_press=lambda widget: start_callback(),style=Pack(padding=5, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb_colors[2], color=rgb_colors[3]))
        self.player_box = toga.Box(style=Pack(direction=COLUMN, padding=5, padding_left=25, padding_right=25))
        self.container = toga.ScrollContainer(content=self.player_box, horizontal=False, style=Pack(flex=1))
        self.name_box = toga.Box(style=Pack(direction=COLUMN, padding=5, flex=1))
        self.name_box.add(self.logo_box)
        self.name_box.add(self.sub_welcome_label)
        self.name_box.add(self.mode_select_container)
        self.name_box.add(self.name_lang_box)
        self.name_box.add(self.container)
        self.name_box.add(self.start_button)

        # The entirety of the task_box and its callbacks:
        def back_button_callback():
            # This basically resets the game
            tools.box_switch(self, self.name_box)
            tools.reset_task_box(self)
            self.counter = 0

        self.task_box = toga.Box(style=Pack(flex=1, direction=COLUMN,))
        self.back_button_box = toga.Box(style=Pack(direction=ROW))
        self.back_button_spacer = toga.Box(style=Pack(flex=1, ))
        self.back_button = toga.Button("⚙", on_press=lambda widget: back_button_callback(), style=Pack(padding=5, font_size=25 , font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb_colors[0], color=rgb(255,255,255)))
        self.back_button_box.add(self.back_button)
        self.back_button_box.add(self.back_button_spacer)
        self.placeholderT = toga.Box(style=Pack(flex=1))
        self.task_label = toga.MultilineTextInput(readonly=True, style=Pack(flex=2, padding=10, padding_top=-50, text_align=CENTER, width=self.main_box.style.width, font_size=24, color=rgb_colors[4], font_family="font",))
        self.button = toga.Button(_("Next"), on_press=lambda widget: tools.task_handler(self), style=Pack(padding=5, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb_colors[2], color=rgb_colors[3]))
        self.task_box.add(self.back_button_box)
        self.task_box.add(self.placeholderT)
        self.task_box.add(self.task_label)
        self.task_box.add(self.button)

        def agree_button_callback():
            self.main_box.remove(self.disclaimer_box)
            self.main_box.add(self.name_box)

        self.disclaimer_box = toga.Box(style=Pack(direction=COLUMN, padding=5, flex=1))
        self.disclaimer_label = toga.MultilineTextInput(readonly=True, style=Pack(flex=1, padding=10, text_align=CENTER, width=self.main_box.style.width, font_size=25, color=rgb_colors[4],))
        self.disclaimer_label.value = _("This drinking game is intended for adults aged 18 and above. Please enjoy responsibly and be mindful of your alcohol consumption. Know your limits, drinking is not a competition! Excessive alcohol consumption can have serious health implications. If you choose to participate, do so in moderation and consider your own tolerance level. Never drink and drive. Cheers responsibly!")
        self.agree_button = toga.Button("I am at least 18 years old", on_press=lambda widget: agree_button_callback(),
                                       style=Pack(padding=5, font_size=25, font_family=SANS_SERIF, font_weight=BOLD,
                                                  background_color=rgb_colors[0], color=rgb(255, 255, 255)))
        self.disclaimer_box.add(self.disclaimer_label)
        self.disclaimer_box.add(self.agree_button)
        # main_box should only ever contain one other box. We use boxes as pages.
        self.main_box.add(self.disclaimer_box)

        self.main_window = toga.MainWindow(title=self.formal_name)
        self.main_window.content = self.main_box
        self.main_window.show()
def main():
    return Trinker(version=0.9)
