# 🌐 Translation

## 📱 App
Translating Trinker is easy! Simply download the "trinker.pot" file and use a program like [POEdit](https://poedit.net/) to start translating it into a new language.

If work has already been done in your language, just open "your_language/LC_MESSAGES" and download the "trinker.po" file instead of "trinker.pot."

Once you're done, you can fork this repo, replace the files you edited, and open a merge request. If you don't feel comfortable using git or the GitLab UI, you can [open an issue](https://gitlab.com/Aarontheissueguy/trinker/-/issues), and we can figure things out together! 🚀

## 🎮 Gamemodes

Gamemodes are language-specific in Trinker. The gamemode folder has a subfolder for each language. Tasks and questions have a special but very easy syntax. To learn more about contributing gamemodes, click [here](#). 🧩
