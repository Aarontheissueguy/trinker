# This file contains a variety of tools that are needed by Trinker
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW, CENTER, BOLD
import random
import os
from . import tasktypes

# box_switch takes a toga box as input, removes every other box from main_box and adds the given box
def box_switch(self, box):
    boxes = [self.task_box, self.name_box]

    for i in boxes:
        if i != box:
            try:
                self.main_box.remove(i)
            except:
                pass

    self.main_box.add(box)

# get_random_task pulls a random task from a gamemode file based on the selection widget selected_mode. It reverts the changes made by selected_mode (see app.py)
def get_random_task(self):
    selected_mode = self.mode_select.value
    if selected_mode.startswith("🎉 "):
        selected_mode = selected_mode[2:]

    if str(self.current_language) in os.listdir(os.path.join(self.app_directory, "gamemodes/")):
        tasks_file_path = os.path.join(self.app_directory, "gamemodes/" + str(self.current_language) + "/" + selected_mode + ".txt")
    else:
        tasks_file_path = os.path.join(self.app_directory, "gamemodes/en/" + selected_mode + ".txt")


    with open(tasks_file_path, 'r') as file:
        tasks = file.readlines()

    # Remove leading and trailing whitespace from each line
    tasks = [task.strip() for task in tasks if task.strip()]

    if self.player_list:
        # Get the name at the current counter index
        chosen_name = self.player_list[self.counter]

        # Increment the counter and reset it to zero if it reaches the end
        self.counter = (self.counter + 1) % len(self.player_list)

        tasks = [task.replace("$NAME", chosen_name) for task in tasks]
    # Why use linebreaks instead of padding? Multiline text is displayed as a white box on gtk. Using linebreaks to lower the text ensures a coherent UI across all platforms.
    return "\n" + random.choice(tasks)

current_language_index = 0
def language():
    languages = ["de", "en"]
    global current_language_index
    current_language = languages[current_language_index]
    print(f"current language: {current_language}")

    # Move to the next language or wrap around to the beginning if at the end of the list
    current_language_index = (current_language_index + 1) % len(languages)

    return current_language

# task_handler checks the task type and points the task to the correct function. If no task type is specified it will simply treat it as a regular task
def task_handler(self):
    task = get_random_task(self)
    if "[CHOICE]" in task:
        tasktypes.multiple_choice(self, task, self.current_language)
    else:
        self.task_label.value = task

# get_player pulls the names from the children of player_box
def get_player_list(self):
    input_values = []
    for widget in self.player_box.children:
        for sub_widget in widget.children:
            for sub_sub_widget in sub_widget.children:
                if isinstance(sub_sub_widget, toga.TextInput):
                    input_values.append(sub_sub_widget.value)
    print(input_values)
    return input_values

# reset_task_box resets the taskbox to its default state
def reset_task_box(self):
    for widget in self.task_box.children:
        self.task_box.remove(widget)
    self.task_box = toga.Box(style=Pack(flex=1, direction=COLUMN, ))
    self.task_box.add(self.back_button_box)
    self.task_box.add(self.placeholderT)
    self.task_box.add(self.task_label)
    self.task_box.add(self.button)

