import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW, CENTER, BOLD, MONOSPACE, SANS_SERIF
import gettext
import os
from toga.colors import rgb
def multiple_choice(self, task, language):
    localedir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'locales'))
    translation = gettext.translation('trinker', localedir, languages=[language])
    translation.install()
    # Removing the Multiple Choice indicator
    output = ""
    for word in task.split("[CHOICE]"):
        output += word
    task = output

    # Extracting the awnsers from our string
    awnsers = task[task.find("{") + 1:-1]
    awnsers_list = awnsers.split(",")

    # Removing the awnsers from our string
    output = ""
    for word in task.split("{" + awnsers + "}"):
        output += word
    task = output

    def choice_callback(awnser):

        print("here")
        if awnser == awnsers_list[4]:
            print("right")
            self.task_box.remove(self.choice_box)
            self.task_label.value = _("Correct! You can choose a person that has to drink.")
        else:
            print("wrong")
            self.task_box.remove(self.choice_box)
            self.task_label.value = _("Wrong! You have to drink.")

        self.task_box.add(self.button)

    self.choice_box = toga.Box(style=Pack(direction=COLUMN, padding=5))
    self.choice_box1 = toga.Box(style=Pack(direction=ROW, padding=5))
    self.choice_box2 = toga.Box(style=Pack(direction=ROW, padding=5))

    choice1 = toga.Button(awnsers_list[0], on_press=lambda widget: choice_callback("1"), style=Pack(padding=2, flex=1, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb(246,217,107), color=rgb(0,0,0)))
    choice2 = toga.Button(awnsers_list[1], on_press=lambda widget: choice_callback("2"), style=Pack(padding=2, flex=1, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb(246,217,107), color=rgb(0,0,0)))
    choice3 = toga.Button(awnsers_list[2], on_press=lambda widget: choice_callback("3"), style=Pack(padding=2, flex=1, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb(246,217,107), color=rgb(0,0,0)))
    choice4 = toga.Button(awnsers_list[3], on_press=lambda widget: choice_callback("4"), style=Pack(padding=2, flex=1, font_family=SANS_SERIF, font_weight=BOLD, background_color=rgb(246,217,107), color=rgb(0,0,0)))

    self.choice_box1.add(choice1)
    self.choice_box1.add(choice2)
    self.choice_box2.add(choice3)
    self.choice_box2.add(choice4)

    self.choice_box.add(self.choice_box1)
    self.choice_box.add(self.choice_box2)

    # Removing the "Next" button to make room for choices
    self.task_box.remove(self.button)

    self.task_box.add(self.choice_box)
    self.task_label.value = task