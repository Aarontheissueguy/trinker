import os

# Set the output .pot file path
potfile = os.path.join(os.path.dirname(__file__), 'locales/trinker.pot')

# List of Python source files to extract translatable strings from
source_files = ['app.py', 'tasktypes.py']

# Check if the .pot file already exists
if not os.path.exists(potfile):
    # Run xgettext to extract translatable strings from source files and create the .pot file
    os.system(f'xgettext --output={potfile} --language=Python --keyword=_ {" ".join(source_files)}')
    print(f"Created '{potfile}' from {', '.join(source_files)}.")
else:
    print(f"'{potfile}' already exists. No new .pot file was created.")
