# 🎮 Contributing Gamemodes

## Syntax for Questions and Tasks

In Trinker, gamemodes are a fun way to engage players in various activities. You can contribute new questions and tasks to gamemodes using a specific syntax. Here's how it works:

- Questions and tasks can be added as new lines to the gamemode files.

- To create a multiple-choice question, add "[CHOICE]" in front of the question or task.

- After adding "[CHOICE]," specify the multiple-choice options like this: `{Option1,Option2,Option3,Option4,CorrectAnswer}` at the end of the question. The last element indicates the correct answer.

- If you want to reference the name of the current player in a question or task, use `$NAME` The app will replace "$NAME" with the player's name during gameplay.

- If a question or task doesn't include a name reference, the player will be simply skipped for that round.

## Examples

Here are some examples of what tasks and questions look like in gamemodes:

```markdown
🎬 $NAME, share three of your favorite movie titles with the group.
👶 $NAME, share a childhood memory to bring back some nostalgic moments.
🤥 Two Truths and a Lie: $NAME chooses a person to tell him/her 2 truths and 1 lie, try to guess which one is the lie; drink if you're wrong.
🙅 Never Have I Ever: Players reveal something they've never done. Others who have done it take a drink. $NAME chooses where to start.
🎭 Charades: $NAME acts out a word or phrase without speaking. Players guess; wrong guesses lead to drinking.
🖌️ Pictionary: $NAME draws a word or phrase. Players guess; incorrect guesses result in drinking.
🗣️ Word Association: Players quickly say words related to a given word. Hesitation or repetition results in drinking. It starts with $NAME and goes clockwise.
📚 Categories: $NAME chooses a category (e.g., fruit types); players go counter-clockwise taking turns naming items in it. Inability to do so leads to drinking.
🎵 Rhyme Time: $NAME comes up with a word; other players go clockwise trying to find a rhyme. Inability to rhyme leads to drinking.
🤔 Would You Rather: $NAME comes up with a Would you rather question; the group votes on the questions, the losers drink.
❓ 20 Questions: $NAME thinks of an object; players ask yes-or-no questions to guess it. Failure to guess leads to drinking.
[CHOICE] 🪐 What is the largest planet in our solar system, $NAME? {Earth,Mars,Jupiter,Venus,3}
[CHOICE] 📜 Who wrote the play "Romeo and Juliet," $NAME? {Charles Dickens,William Shakespeare,Jane Austen,Leo Tolstoy,2}
[CHOICE] 🥇 What is the chemical symbol for gold, $NAME? {Gd,Go,Au,Ag,3}
```

## Gamemode Files

Gamemodes are language-specific and are saved as text files with the naming convention "gamemode_name.txt." in their language specific folder.

When you create or modify a gamemodetxt file, ensure that you adhere to the syntax guidelines provided. This ensures that the app can automatically recognize and utilize your gamemode.

To make a contribution by adding a new gamemode, initiate the process by creating a gamemodetxt file with a fitting name and then proceed to incorporate your questions and tasks, keeping them in line with the provided syntax.

If you don not like a question or think its not fitting, you may also suggest to remove it.

If you don't feel comfortable using the syntax or working with Git, don't worry! You can simply [open an issue](https://gitlab.com/Aarontheissueguy/trinker/-/issues) with your suggestions.
