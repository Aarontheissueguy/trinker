    🤒 Ich habe noch nie krank von der Arbeit gefehlt.
    🌊 Ich bin noch nie nackt baden gegangen.
    💔 Ich habe noch nie meinen Partner betrogen.
    📝 Ich habe noch nie bei einem Test geschummelt.
    🗣️ Ich habe noch nie an einem Protestmarsch teilgenommen.
    💳 Ich habe noch nie mein Bankkonto überzogen.
    🍱 Ich habe noch nie das Essen eines anderen aus dem Bürokühlschrank gegessen.
    🃏 Ich habe noch nie Strip-Poker gespielt.
    🌿 Ich habe noch nie einen Joint geraucht.
    🏊‍ Ich habe noch nie in einen Pool gepinkelt.
    💉 Ich habe noch nie harte Drogen ausprobiert.
    😴 Ich bin noch nie in der Öffentlichkeit eingeschlafen.
    💤 Ich bin noch nie bei der Arbeit eingeschlafen.
    📄 Ich habe noch nie in meinem Lebenslauf gelogen.
    📞 Ich habe noch nie meinen Ex im betrunkenen Zustand angerufen.
    🌀 Ich habe noch nie Acid (LSD) genommen.
    🛌 Ich hatte noch nie einen One-Night-Stand.
    🔍 Ich habe noch nie die SMS meines Partners gelesen.
    ✉️ Ich habe noch nie die E-Mails meines Partners gelesen.
    🏥 Ich wurde noch nie wegen etwas anderes als einer Geburt oder Entbindung ins Krankenhaus eingeliefert.
    🎤 Ich habe noch nie in der Öffentlichkeit gesungen.
    🎵 Ich habe noch nie ein Musikinstrument gespielt.
    ❄️ Ich bin noch nie Snowboarden gegangen.
    ⛷️ Ich bin noch nie Ski gefahren.
    ✈️ Ich bin noch nie in ein anderes Land gereist.
    🤐 Ich habe noch nie ein Geheimnis verschwiegen, das jemandes Leben stark beeinflusst hätte.
    📸 Ich habe noch nie an einem unangemessenen Ort ein Selfie gemacht.
    🎭 Ich habe noch nie vor Publikum auf einer Bühne aufgetreten.
    🚓 Ich wurde noch nie in Handschellen gelegt oder von der Polizei festgehalten.
    💍 Ich habe noch nie ein wertvolles Schmuckstück verloren.
    🚗 Ich habe noch nie einen Strafzettel wegen zu schnellen Fahrens bekommen.
    🚫 Ich habe noch nie einen ganzen Tag ohne mein Handy ausgehalten.
    🤥 Ich habe noch nie so getan, als würde ich etwas über ein Thema wissen, von dem ich keine Ahnung hatte.
    🍕 Ich habe noch nie Essen gegessen, das auf den Boden gefallen ist, ohne zu zögern.
    📚 Ich habe noch nie so getan, als hätte ich ein Buch gelesen oder einen Film gesehen, den ich tatsächlich nicht gelesen oder gesehen habe.
    🆔 Ich habe noch nie einen gefälschten Ausweis benutzt.
    🚔 Ich bin noch nie verhaftet worden.
    💔 Ich habe mich noch nie bei einem Date gedemütigt.
    👃 Mir ist noch nie Essen aus der Nase gekommen.
    📝 Ich habe noch nie bei einem Test geschummelt.
    🛌 Ich habe noch nie nackt geschlafen.
    💌 Ich habe noch nie einen Akt erhalten.
    🍸 Ich war noch nie beim ersten Date zu betrunken.
    🦷 Ich habe noch nie die Zahnbürste eines anderen benutzt.
    💅 Ich habe noch nie an meinen Fingernägeln gekaut.
    👣 Ich habe noch nie an meinen Zehennägeln gebissen.
    🍬 Ich habe noch nie einen Kaugummi herausgenommen und ihn „für später“ irgendwo hingesteckt.
    🍔 Ich habe noch nie Essen gegessen, das gegen die Fünf-Sekunden-Regel verstoßen hat.
    🗣️ Ich habe noch nie so getan, als hätte ich einen Akzent.
    🚽 Ich habe noch nie mein Handy in die Toilette fallen lassen.
    🪱 Ich habe noch nie einen Wurm berührt.
    👔 Ich war noch nie in einem Geschäft für Erwachsene.
    🍸 Ich habe noch nie mit jemandem geflirtet, um ein Freigetränk zu bekommen.
    🤢 Ich habe mich noch nie betrunken auf einen Fremden übergeben.
    🚗 Ich bin noch nie nackt Auto gefahren.
    🚫 Ich habe noch nie mehr als zweimal mit dem Trinken aufgehört.
    🚬 Ich habe noch nie mehr als zweimal mit dem Rauchen aufgehört.
    🏊‍ Ich bin noch nie nackt im Pool eines anderen geschwommen.
    🌳 Ich bin noch nie ohne Kleidung nach draußen gegangen.
    💸 Ich habe noch nie für Inhalte für Erwachsene bezahlt.
    👪 Ich habe noch nie meine Eltern hinters Licht geführt.
    💃 Ich habe noch nie auf einem Tisch getanzt.
    🤢 Ich bin noch nie verkatert zur Arbeit gegangen.